/* eslint-disable no-console */
import chalk from 'chalk';
import pj from 'prettyjson';

const chekEnv = () => {
    switch (process.env.NODE_ENV) {
    case 'test':
    case 'silent':
        return false;
    default:
        return true;
    }
};

const writeMessage = (message, color) => {
    console.log(color(pj.render(message)));
};

const log = message => writeMessage(message, chalk.blue);

const info = message => writeMessage(message, chalk.cyan);

const error = message => writeMessage(message, chalk.red);

const warn = message => writeMessage(message, chalk.yellow);

export default {
    log,
    info,
    error,
    warn
};

export {log, info, error, warn, chekEnv};
