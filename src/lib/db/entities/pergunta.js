import mongoose, {Schema} from 'mongoose';

mongoose.Promise = global.Promise;

// Criação do Schema
const jsonSchema = {
    msg_id: Number,
    text: String,
    sent_date: {type: Date, default: Date.now},
    answers: [
        {
            aluno: {type: Schema.Types.ObjectId, ref: 'Aluno'},
            resposta: String
        }
    ]
};

const perguntaSchema = new Schema(jsonSchema);

const Pergunta = mongoose.model('Pergunta', perguntaSchema, 'perguntas');

const Controller = {
    insert: d => {
        return new Promise((res, rej) => {
            const msg_id = d.msg_id;
            Pergunta.findOne({msg_id}, (err, doc) => {
                if (err)
                    rej(err);
                else if (!doc) {
                    new Pergunta(d).save((err, data) => {
                        if (err) rej(err);
                        else res(data);
                    });
                } else {
                    doc.msg_id = d.msg_id;
                    doc.text = d.text;
                    doc.sent_date = d.sent_date;
                    if (d.answers) doc.answers = d.answers;
                    doc.save((err, saved) => {
                        if (err) rej(err);
                        else res(saved);
                    });
                }
            });
        });
    },
    select: q => {
        return new Promise((res, rej) => {
            Pergunta.find(q, (err, data) => {
                if (err) rej(err);
                else res(data);
            });
        });
    },
    delete: q => {
        return new Promise((res, rej) => {
            Pergunta.remove(q, (err, data) => {
                if (err) rej(err);
                else res(data);
            });
        });
    },
    answer: (p, a) => {
        return new Promise((res, rej) => {
            Pergunta.findOne({_id: p._id}, (err, doc) => {
                if (err)
                    rej(err);
                else if (doc) {
                    doc.answers.push(a);
                    doc.save((err, data) => {
                        if (err) rej(err);
                        else res(data);
                    });
                } else {
                    rej({message: 'Pergunta não encontrada!'});
                }
            });
        });
    }
};

module.exports = Controller;
