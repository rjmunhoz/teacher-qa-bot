import mongoose, {Schema} from 'mongoose';

mongoose.Promise = global.Promise;

// Criação do Schema
const jsonSchema = {
    id: Number,
    first_name: String,
    last_name: String,
    username: String
};

const alunoSchema = new Schema(jsonSchema);

const Aluno = mongoose.model('Aluno', alunoSchema, 'alunos');

const Controller = {
    insert: d => {
        return new Promise((res, rej) => {
            const id = d.id;
            Aluno.findOne({id}, (err, doc) => {
                if (err)
                    rej(err);
                else if (!doc) {
                    new Aluno(d).save((err, data) => {
                        if (err) rej(err);
                        else res(data);
                    });
                } else {
                    res(false);
                }
            });
        });
    },
    select: q => {
        return new Promise((res, rej) => {
            Aluno.find(q, (err, data) => {
                if (err) rej(err);
                else res(data);
            });
        });
    },
    find: q => {
        return new Promise((res, rej) => {
            Aluno.findOne(q, (err, data) => {
                if (err) rej(err);
                else res(data);
            });
        });
    },
    delete: q => {
        return new Promise((res, rej) => {
            Aluno.remove(q, (err, data) => {
                if (err) rej(err);
                else res(data);
            });
        });
    },
    selectOrCreate: a => {
        return new Promise((res, rej) => {
            Aluno.findOne(a, (err, doc) => {
                if (err) {
                    rej(err);
                } else if (doc) {
                    res(doc);
                } else {
                    new Aluno(a).save((err, saved) => {
                        if (err) rej(err);
                        else res(saved);
                    });
                }
            });
        });
    }
};

module.exports = Controller;
