import mongoose from 'mongoose';
import {error, log, info} from '../utils/log';
import aluno from './entities/aluno';
import pergunta from './entities/pergunta';
import dotenv from 'dotenv-safe';

dotenv.load();

const MONGO_DATABASE = process.env.MONGO_DATABASE;
const MONGO_ADDR = process.env.MONGO_ADDR || 'localhost';
const MONGO_AUTH = `${process.env.MONGO_AUTH}@` || '';

const connString = `mongodb://${MONGO_AUTH}${MONGO_ADDR}/${MONGO_DATABASE}`;

mongoose.Promise = global.Promise;
mongoose.connect(connString);

mongoose.connection.on('error', function(err) {
    error('Mongoose default connection error: ' + err);
});
mongoose.connection.on('disconnected', function() {
    log('Mongoose default connection disconnected');
});
mongoose.connection.on('open', function() {
    info('Mongoose default connection is open');
});

process.on('SIGINT', function() {
    mongoose.connection.close(function() {
        log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});

export {aluno, pergunta};
