import {aluno} from '../db';
import Pergunta from '../entities/Pergunta';


const questionSaved = (msg, p) => {
    const _func = bot =>
        bot.sendMessage(
            msg.chat.id,
            `Pergunta ${p.message_id} salva no banco e enviada aos alunos`
        );

    return _func;
};

const handle = msg => new Promise((res, rej) => {
    const pergunta = new Pergunta(msg.message_id, msg.text);
    pergunta
        .save()
        .then(p => {
            aluno
                .select({})
                .then(alunos => {
                    if (alunos && Array.isArray(alunos)) {
                        const reply = alunos.map(a => {
                            return bot => bot.sendMessage(a.id, `Nova pergunta:\n*"${msg.text}"*\nPor favor, dê reply nesta mensagem para responder, e bons estudos! ;)\n#${msg.message_id}`, {parse_mode: 'Markdown'});
                        });
                        reply.push(questionSaved(msg, p));
                        res(reply);
                    }
                })
                .catch(rej);
        })
        .catch(rej);
});

export default handle;
