/* eslint-disable no-unused-vars */
import {aluno} from '../db';
import Pergunta from '../entities/Pergunta';

const saveAnswer = (msg, res, rej) => {
    const msg_id = msg.reply_to_message.text.match(/#([0-9]+)/)[1];
    Pergunta.fromMessage(msg_id).then(p => {
        aluno.find({id: msg.from.id}).then(a => {
            p
                .answer({
                    aluno: a._id,
                    resposta: msg.text
                })
                .then(() => {
                    res(bot =>
                        bot.sendMessage(
                            msg.chat.id,
                            'Resposta armazenada no banco. Obrigado!'
                        ));
                })
                .catch(rej);
        });
    })
        .catch(rej);
};

const saveStudent = (msg, res, rej) => {
    aluno
        .find({id: msg.from.id})
        .then(a => {
            if (a) {
                res(bot =>
                    bot.sendMessage(
                        msg.chat.id,
                        'Por favor, dê reply em uma mensagem contendo uma pergunta do professor!'
                    ));
            } else {
                aluno
                    .insert(msg.from)
                    .then(
                        res(bot =>
                            bot.sendMessage(
                                msg.chat.id,
                                'Ok, salvei seu ID aqui e, assim que chegar uma nova pergunta, eu te encaminho =)'
                            ))
                    )
                    .catch(rej);
            }
        })
        .catch(rej);
};

const handle = msg => new Promise((res, rej) => {
    if (msg.reply_to_message) {
        saveAnswer(msg, res, rej);
    } else {
        saveStudent(msg, res, rej);
    }
});

export default handle;
