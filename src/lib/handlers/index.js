import fromProfessor from './fromProfessor';
import fromAluno from './fromAluno';
import admins from '../../admins';

const handle = msg => {
    if (msg.from.id in admins) {
        return fromProfessor(msg);
    } else {
        return fromAluno(msg);
    }
};

export default {handle};
