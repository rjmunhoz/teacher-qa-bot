import {pergunta} from '../db';

export default class Pergunta {
    constructor(message_id, text) {
        this.message_id = message_id;
        this.text = text;
        this.sent_date = Date.now();
    }

    get id() {
        return this._id;
    }

    set id(id) {
        if(!id) throw new TypeError('Invalid id!');
        this._id = id;
    }

    get message_id() {
        return this._message_id;
    }

    set message_id(message_id) {
        if(!message_id) throw new TypeError('Invalid message_id!');
        this._message_id = message_id;
    }

    get text() {
        return this._text;
    }

    set text(text) {
        if(!text) throw new TypeError('Invalid text!');
        this._text = text;
    }

    get sent_date() {
        return this._sent_date;
    }

    set sent_date(sent_date) {
        if(!sent_date) throw new TypeError('Invalid date!');
        this._sent_date = sent_date;
    }

    answer(a) {
        return pergunta.answer({_id: this.id}, a);
    }

    save() {
        return new Promise((res, rej) => {
            pergunta
                .insert(this.toDbObj())
                .then(result => {
                    this.id = result._id;
                    res(this);
                })
                .catch(rej);
        });
    }

    toDbObj() {
        const _pergunta = {
            msg_id: this.message_id,
            text: this.text,
            sent_date: this.sent_date
        };

        if (this.id) _pergunta._id = this.id;

        return _pergunta;
    }

    static fromMessage(msg_id) {
        return new Promise((res, rej) => {
            pergunta
                .select({msg_id})
                .then(p => {
                    if (p.length > 0) {
                        p = p[0];
                        const _pergunta = new Pergunta(p.msg_id, p.text);
                        _pergunta.id = p._id;
                        _pergunta.sent_date = p.sent_date;
                        _pergunta.text = p.text;
                        res(_pergunta);
                    } else {
                        rej({message: `Pergunta ${msg_id} não encontrada!`});
                    }
                })
                .catch(rej);
        });
    }
}
