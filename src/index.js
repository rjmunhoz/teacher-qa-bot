import TelegramBot from 'node-telegram-bot-api';
import {info, error} from './lib/utils/log';
import dotenv from 'dotenv-safe';
import handlers from './lib/handlers';

dotenv.load();

const bot = new TelegramBot(process.env.TELEGRAM_TOKEN, {
    polling: true,
    onlyFirstMatch: true
});

bot.getMe().then(me => {
    const _info = [];
    _info.push('==========================');
    _info.push('Bot successfully deployed!');
    _info.push(`Username: ${me.username}`);
    _info.push('==========================');
    info(_info.join('\n'));
});

bot.on('text', msg => {
    if (!msg.text.startsWith('/')) {
        handlers
        .handle(msg)
        .then(reply => {
            if (Array.isArray(reply)) {
                reply.forEach(r => {
                    r(bot).catch(error);
                });
            } else {
                reply(bot).catch(error);
            }
        })
        .catch(error);
    }
});

bot.onText(/\/start (.*)/, msg =>
    bot.sendMessage(msg.chat.id, 'Eae :P Você já deve saber como me usar, então... Partiu! xD'));

bot.onText(/\/.*/, msg =>
    bot.sendMessage(msg.chat.id, 'Nada de comandos por aqui, jovem xD'));
